# vue and element-demo
###  vue结合element-ui的demo
> A Vue.js project
> 高仿饿了么移动端，采用真实的饿了么移动端数据，
#### 使用的组件,vue全家桶,element-ui
- vue.js
- vue-router
- vue-axios
- element-ui
- es6

> 项目使用webpack打包发布

### 部分预览
![ALT TEXT](/screenShot/location.gif "首页")

## Build Setup

``` bash
# 安装项目依赖
npm install

# 启动本地服务,并运行在8080端口
npm run dev

# 打包并压缩发布项目文件
npm run build
```
