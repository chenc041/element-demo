// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import axios from 'axios';
import VueAxios from 'vue-axios';
import ElementUI from 'element-ui';
import { Swipe, SwipeItem } from 'vue-swipe';
import 'element-ui/lib/theme-default/index.css';
import 'vue-swipe/dist/vue-swipe.css';

import './assets/stylesheets/normalize.css';
import './assets/stylesheets/common.css';

Vue.use(VueAxios, axios);
Vue.use(ElementUI);
Vue.component('swipe', Swipe);
Vue.component('swipe-item', SwipeItem);

/**
 * 初始化路由
 */

/* eslint-disable no-new */
new Vue({
    el: '#app',
    template: '<App/>',
    components: { App },
    router,
});
