/**
 * Created by watermelonMan on 2017/3/27.
 */
import vue from 'vue';
import VueRouter from 'vue-router';
import List from '../components/List';
import CitySearch from '../components/CitySearch';
import Shop from '../components/Shop';
import Login from '../components/Login';

vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: List,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/city/:CitySearchId',
            name: 'CitySearch',
            component: CitySearch,
        },
        {
            path: '/shop/:shop_geo',
            name: 'shop_geo',
            component: Shop,
        },
    ],
});
